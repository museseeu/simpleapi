json.extract! store, :id, :title, :des, :address, :phone, :owner, :created_at, :updated_at
json.url store_url(store, format: :json)

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


20.times do 
  Store.create(
    title: Faker::Book.title,
    des: Faker::Company.catch_phrase,
    address: Faker::Address.city,
    phone: Faker::Number.number(5),
    owner: Faker::Book.publisher
  )
end

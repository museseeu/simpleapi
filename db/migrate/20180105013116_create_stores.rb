class CreateStores < ActiveRecord::Migration[5.1]
  def change
    create_table :stores do |t|
      t.string :title
      t.string :des
      t.string :address
      t.integer :phone
      t.string :owner

      t.timestamps
    end
  end
end
